FROM php:8.2-alpine

# Web application
RUN wget -O limesurvey.tar.gz https://github.com/LimeSurvey/LimeSurvey/archive/refs/tags/6.6.0+240729.tar.gz && \
    tar -xzf limesurvey.tar.gz && \
    mv LimeSurvey-* /app && \
    rm limesurvey.tar.gz

# Harmonise www-data uid with Ubuntu, as we mount storage from Ubuntu inside
RUN deluser www-data && adduser -D -u 33 -s /bin/false www-data

# Scripts
COPY ./scripts/run.sh /scripts/run.sh
RUN chmod -R 755 /scripts

# Install dependencies
RUN apk update \
    && apk add --no-cache \
    php82 \
    php82-apache2 \
    php82-ctype \
    php82-dom \
    php82-fileinfo \
    php82-gd \
    php82-iconv \
    php82-imap \
    php82-json \
    php82-ldap \
    php82-mysqli \
    php82-mbstring \
    php82-openssl \
    php82-pdo_mysql \
    php82-session \
    php82-simplexml \
    php82-xml \
    php82-xmlwriter \
    php82-zip \
    php82-zlib \
    gettext \
    iputils \
    nano \
    perl-net-telnet \
    ssmtp

RUN docker-php-ext-install mysqli pdo pdo_mysql

# Data directory
RUN mkdir /app-data && mkdir /app/log && mkdir -p /run/apache2
RUN chown -R www-data:www-data /app/tmp && chown -R www-data:www-data /app/upload && chown www-data:www-data /app/application && chown www-data:www-data /app/admin && chown -R www-data:www-data /app/application/config && chown www-data:www-data /run/apache2

# Pleio style
COPY ./config/style/custom.css /app/themes/survey/fruity/css/custom.css
COPY ./config/style/pleio_logo.png /app/themes/survey/fruity/files/logo.png

# Configuration files
COPY ./config/apache2.conf /etc/apache2/conf.d/custom.conf
COPY ./config/php.ini /etc/php82/conf.d/100_custom.ini
COPY ./config/ssmtp.conf /etc/ssmtp/ssmtp.conf.template

COPY ./config/limesurvey-config.php /app/application/config/config.php

WORKDIR /app
EXPOSE 80

CMD ["/scripts/run.sh"]
