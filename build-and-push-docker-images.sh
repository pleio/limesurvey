#!/bin/bash
set -e

docker build \
-t registry.gitlab.com/pleio/limesurvey:latest \
-t registry.gitlab.com/pleio/limesurvey:$(git rev-parse --short HEAD) \
.

docker push registry.gitlab.com/pleio/limesurvey:latest
docker push registry.gitlab.com/pleio/limesurvey:$(git rev-parse --short HEAD)

