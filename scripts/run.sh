#!/bin/sh

# Configure default environment variables
export SMTP_HOST=email-smtp.eu-west-1.amazonaws.com:465
export SMTP_HOSTNAME=pleio.nl

echo "[i] Configuring ssmtp..."
envsubst < /etc/ssmtp/ssmtp.conf.template > /etc/ssmtp/ssmtp.conf

echo "[i] Starting daemon..."
httpd

# display logs
tail -F /var/log/apache2/*log