# Docker build scripts for Filesender

Run limsesurvey locally with docker-compose.
```bash
docker-compose up --build
```

For first-timers run the installation script inside the container to initialize the database
```
docker exec -it limseurvey_limsurvey_1 sh
# inside the container
cd application/commands
php console.php install <admin-username> <admin-password> <admin-name> <admin-email>
```
